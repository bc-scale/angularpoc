import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ColDef, GridApi, GridReadyEvent, ICellRendererParams, ValueFormatterParams } from 'ag-grid-community';
import { Observable } from 'rxjs';
import { BtnDeleteRendererComponent } from '../renderer/btn-delete-renderer';
import { BtnComputerIDRendererComponent } from '../renderer/btn-computerid-renderer';

@Component({
selector: 'app-root',
templateUrl: './app.component.html',
styleUrls: ['./app.component.css']
})

export class AppComponent{
  private gridApi!: GridApi;
  title = "aggrid-formacion";
  searchFilter = '';
  sidebar: any;

public columnDefs: ColDef[] = [
{ headerName: 'ID', field: 'ID', hide: true},
{ headerName: 'Hostname', field: 'hostname', filter: 'agTextColumnFilter', floatingFilter: true },
{ headerName: 'Assigned IP', field: 'ip', filter: 'agTextColumnFilter', floatingFilter: true },
{ headerName: 'Date of Purchase', field: 'purchase_date', filter: 'agDateColumnFilter', floatingFilter: true, cellStyle: (params: any) => {
  const str_date = params.value.toString();
  var year = str_date.substring(6,11)
  var this_year = (new Date()).getFullYear();  
  if (this_year-year > 4) {
  return {'background-color': 'green'}
  } else {
  return
  }
}},
{ headerName: 'Office',field: 'office', filter: 'agTextColumnFilter', floatingFilter: true},
{ headerName: 'OS',field: 'os', filter: 'agTextColumnFilter', floatingFilter: true},
{ headerName: 'GB HD',field: 'gb', valueFormatter: gbFormatter, filter: 'agNumberColumnFilter', floatingFilter: true},
{ headerName: 'Computer ID', field: "btnID", cellRenderer: BtnComputerIDRendererComponent},
{ headerName: 'Remove', field: "btnDelete", cellRenderer: BtnDeleteRendererComponent }
];

public defaultColDef: ColDef = {
sortable: true,
filter: true,
enableRowGroup: true
};

public rowData$!: Observable<any[]>;

constructor(private http: HttpClient) {}

  onFilterTextBoxChanged() {
    this.gridApi.setQuickFilter(
      (document.getElementById('filter-text-box') as HTMLInputElement).value
    );
  }

  onGridReady(params: GridReadyEvent) {
    this.rowData$ = this.http
      .get<any[]>('assets/inventary.json');
  }  

}

function gbFormatter(params: ValueFormatterParams) {
    return params.value + ' GB';
  }

