import {Component, Output, EventEmitter} from '@angular/core';
import {ICellRendererAngularComp} from 'ag-grid-angular';
import { ColDef, GridApi, GridReadyEvent, ICellRendererParams, ValueFormatterParams } from 'ag-grid-community'; 
@Component({
  selector: 'app-child-cell',
  template: '<button (click)="btnClickedHandler()">Delete</button>',
})

export class BtnDeleteRendererComponent implements ICellRendererAngularComp {
  public params!: any;

  agInit(params: any): void {
    this.params = params;
  }

  btnClickedHandler() {
    this.deleteMethod()
    alert(`This Computer Removed! `);
  }

  refresh(): boolean {
    return true;
  }

  public deleteMethod() {
    //this.params.context.componentParent.deleteMethod(this.params);
    //const sel = gridOptions.api.getSelectedRows();
    //gridOptions.api.applyTransaction({remove: sel});
    this.params.api.applyTransaction({remove: [this.params.node.data]})
  }
}
